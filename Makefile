# This file is taken (and adapted) from the documentation page of
# coq_makefile:
# https://coq.inria.fr/distrib/current/refman/practical-tools/utilities.html#building-coq-project



# KNOWNTARGETS will not be passed along to CoqMakefile
KNOWNTARGETS := CoqMakefile _CoqProject
# KNOWNFILES will not get implicit targets from the final rule, and so
# depending on them won't invoke the submake
# Warning: These files get declared as PHONY, so any targets depending
# on them always get rebuilt
KNOWNFILES   := Makefile _CoqProject

.DEFAULT_GOAL := invoke-coqmakefile

CoqMakefile: Makefile _CoqProject
	$(COQBIN)coq_makefile -f _CoqProject -o CoqMakefile

invoke-coqmakefile: CoqMakefile
	$(MAKE) --no-print-directory -f CoqMakefile $(filter-out $(KNOWNTARGETS),$(MAKECMDGOALS))

.PHONY: invoke-coqmakefile $(KNOWNFILES)

####################################################################
##                      Your targets here                         ##
####################################################################

_CoqProject:
	echo '-R src Michocoq_proofs' > $@
	find src -iname "*.v" >> $@

# This should be the last rule, to handle any targets not declared above
%: invoke-coqmakefile
	@true
