From Michocoq Require main error.
From Michocoq Require Import syntax.
Require script_string.

Definition script_M := Eval cbv in
      main.contract_file_M script_string.contract_s 500.

Definition script := Eval cbv in error.extract script_M I.

Definition parameter_ty := Eval cbv in contract_file_parameter script.

Definition self_ty : self_info := Some (parameter_ty, None).

Definition storage_ty := Eval cbv in
      contract_file_storage script.

(* The script does an UNPAIR 6 on the storage before branching on the parameter so the input stack for
   the entrypoints is not ((pair parameter_ty storage_ty) ::: nil) as we might expect.
*)
Definition entrypoint_input_stack :=
  address ::: set address ::: nat ::: nat :::
          big_map nat (pair timestamp (pair (lambda unit (list operation)) (set address))) :::
          big_map string bytes ::: list operation ::: nil.
