(* Open Source License *)
(* Copyright (c) 2022 Nomadic Labs. <contact@nomadic-labs.com> *)

(* Permission is hereby granted, free of charge, to any person obtaining a *)
(* copy of this software and associated documentation files (the "Software"), *)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense, *)
(* and/or sell copies of the Software, and to permit persons to whom the *)
(* Software is furnished to do so, subject to the following conditions: *)

(* The above copyright notice and this permission notice shall be included *)
(* in all copies or substantial portions of the Software. *)

(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER *)
(* DEALINGS IN THE SOFTWARE. *)

Require Import ZArith.
Require Import Lia.

From Michocoq Require Import syntax.
From Michocoq Require parser_helpers macros.
Require parsing.
Require specification.
Require Export contract_semantics.

Require set_metadata_uri_correct.
Require default_correct.
Require propose_correct.
Require approve_correct.
Require execute_correct.
Require archive_correct.

Definition storage_ty := parsing.storage_ty.
Definition parameter_ty := parsing.parameter_ty.
Definition self_ty := parsing.self_ty.

Definition code : full_contract false parameter_ty None storage_ty :=
  Eval cbv in contract_file_code parsing.script.

Theorem multisig_managed_state_correct fuel env (new_storage : data storage_ty) (emmited_operations : data (list operation)) (arg : data (entrypoints.entrypoint_tree_to_type parameter_ty)) (old_storage : data storage_ty) :
  fuel >= 2 ->
  eval_seq env code (4 + fuel) ((arg, old_storage), tt) = error.Return ((emmited_operations, new_storage), tt)
  <->
  specification.contract_specification self_ty env fuel arg old_storage emmited_operations new_storage.
Proof.
  intro Hfuel. unfold ">=" in Hfuel.
  change code with
      {NIL operation;
       DIG 1 (S1 := _ ::: nil) eq_refl;
       UNPAIR;
       macros.DIP1 { UNPAIRN 6 (S1 := _ ::: _ ::: _ ::: _ ::: nil) eq_refl };
       IF_LEFT
         { IF_LEFT set_metadata_uri_correct.ep default_correct.ep }
         { IF_LEFT
             { IF_LEFT propose_correct.ep approve_correct.ep }
             { IF_LEFT execute_correct.ep archive_correct.ep}}}%michelson.

  remember set_metadata_uri_correct.ep as set_metadata_uri.
  remember default_correct.ep as default.
  remember propose_correct.ep as propose.
  remember approve_correct.ep as approve.
  remember execute_correct.ep as execute.
  remember archive_correct.ep as archive.

  rewrite error.return_precond.
  rewrite eval_seq_precond_correct.
  simpl.

  unfold specification.contract_specification.
  destruct arg as [[arg|[]]|[[(action, duration)|arg]|[arg|arg]]]; simpl.
  - repeat rewrite fold_eval_precond.
    rewrite fold_eval_seq_precond_aux.
    rewrite <- eval_seq_precond_correct.
    subst set_metadata_uri.
    rewrite eval0_seq_precond_enough.
    + destruct old_storage as (owner, (signers, (threshold, (last_op_id, (pending_ops, metadata))))).
      exact (set_metadata_uri_correct.set_metadata_uri_correct env new_storage emmited_operations arg (owner, (signers, (threshold, (last_op_id, (pending_ops, metadata)))))).
    + compute.
      lia.
  - repeat rewrite fold_eval_precond.
    rewrite fold_eval_seq_precond_aux.
    rewrite <- eval_seq_precond_correct.
    subst default.
    rewrite eval0_seq_precond_enough.
    + destruct old_storage as (owner, (signers, (threshold, (last_op_id, (pending_ops, metadata))))).
      exact (default_correct.default_correct env new_storage emmited_operations tt (owner, (signers, (threshold, (last_op_id, (pending_ops, metadata)))))).
    + compute.
      lia.
  - rewrite fold_eval_precond.
    rewrite fold_eval_seq_precond_aux.
    rewrite <- eval_seq_precond_correct.
    subst propose.
    rewrite eval0_seq_precond_enough.
    + destruct old_storage as (owner, (signers, (threshold, (last_op_id, (pending_ops, metadata))))).
      exact (propose_correct.propose_correct env new_storage emmited_operations (action, duration) (owner, (signers, (threshold, (last_op_id, (pending_ops, metadata)))))).
    + compute.
      lia.
  - rewrite fold_eval_precond.
    rewrite fold_eval_seq_precond_aux.
    rewrite <- eval_seq_precond_correct.
    subst approve.
    rewrite eval0_seq_precond_enough.
    + destruct old_storage as (owner, (signers, (threshold, (last_op_id, (pending_ops, metadata))))).
      exact (approve_correct.approve_correct env new_storage emmited_operations arg (owner, (signers, (threshold, (last_op_id, (pending_ops, metadata)))))).
    + compute.
      lia.
  - rewrite fold_eval_precond.
    rewrite fold_eval_seq_precond_aux.
    subst execute.
    destruct old_storage as (owner, (signers, (threshold, (last_op_id, (pending_ops, metadata))))).
    do 2 more_fuel.
    exact (execute_correct.execute_correct (S fuel) env new_storage emmited_operations arg (owner, (signers, (threshold, (last_op_id, (pending_ops, metadata)))))).
  - repeat rewrite fold_eval_precond.
    rewrite fold_eval_seq_precond_aux.
    rewrite <- eval_seq_precond_correct.
    subst archive.
    rewrite eval0_seq_precond_enough.
    + destruct old_storage as (owner, (signers, (threshold, (last_op_id, (pending_ops, metadata))))).
      exact (archive_correct.archive_expired_operation_correct env new_storage emmited_operations arg (owner, (signers, (threshold, (last_op_id, (pending_ops, metadata)))))).
    + compute.
      lia.
Qed.
