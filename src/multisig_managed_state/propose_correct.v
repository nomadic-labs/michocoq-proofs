(* Open Source License *)
(* Copyright (c) 2022 Nomadic Labs. <contact@nomadic-labs.com> *)

(* Permission is hereby granted, free of charge, to any person obtaining a *)
(* copy of this software and associated documentation files (the "Software"), *)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense, *)
(* and/or sell copies of the Software, and to permit persons to whom the *)
(* Software is furnished to do so, subject to the following conditions: *)

(* The above copyright notice and this permission notice shall be included *)
(* in all copies or substantial portions of the Software. *)

(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER *)
(* DEALINGS IN THE SOFTWARE. *)

Require Import ZArith.

From Michocoq Require Import syntax.
From Michocoq Require parser_helpers.
Require parsing.
Require specification.
Require Import contract_semantics.

Definition storage_ty := parsing.storage_ty.
Definition parameter_ty := parsing.parameter_ty.
Definition self_ty := parsing.self_ty.

Definition ep_input_stack := parsing.entrypoint_input_stack.

Definition ep_annot := specification.propose_annot.

Definition ep_parameter_ty := Eval cbv in
      error.opt_extract
        (entrypoints.get_entrypoint_opt ep_annot parameter_ty None)
        I.

Definition ep_M : error.M (instruction_seq self_ty false (ep_parameter_ty ::: ep_input_stack) (pair (list operation) storage_ty ::: nil)) :=
  Eval cbv in
     parser_helpers.string_to_instr_seq script_string.ep_propose 500 _ _ _.

Definition ep : instruction_seq self_ty false (ep_parameter_ty ::: ep_input_stack) (pair (list operation) storage_ty ::: nil) :=
  Eval cbv in error.extract ep_M I.

Lemma propose_correct env (new_storage : data storage_ty) (emmited_operations : data (list operation)) (arg : data ep_parameter_ty) (old_storage : data storage_ty) :
  let '(owner,
        (signers,
         (threshold,
          (last_op_id,
           (pending_ops,
            metadata))))) := old_storage in
  let '(action, duration) := arg in
  eval0_seq_precond env ep (fun SA => SA = ((emmited_operations, new_storage), tt)) (arg, (owner, (signers, (threshold, (last_op_id, (pending_ops, (metadata, (nil, tt)))))))) <->
  specification.propose_spec
    self_ty env action duration
    (specification.read_storage old_storage)
    emmited_operations
    (specification.read_storage new_storage).
Proof.
  destruct old_storage as (owner, (signers, (threshold, (last_op_id, (pending_ops, metadata))))).
  destruct arg as (action, duration).
  unfold specification.propose_spec,
  specification.storage_invariant_spec.
  destruct new_storage as (owner', (signers', (threshold', (last_op_id', (pending_ops', metadata'))))).
  simpl.

  rewrite Bool.negb_false_iff.
  rewrite Bool.negb_false_iff.
  rewrite Bool.negb_false_iff.
  rewrite (comparable.eqb_eq mutez).
  rewrite (comparable.gtb_gt_timestamp).

  split.
  - intros (H1, (H2, (H3, (H4, H5)))). injection H5. intros. subst.
    intuition.
  - intros (H1, (H2, (H3, (H4, ((H5, (H6, (H7, H8))), (H9, (H10, H11))))))).
    subst.
    intuition.
Qed.
