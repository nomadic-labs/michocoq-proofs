(* Open Source License *)
(* Copyright (c) 2022 Nomadic Labs. <contact@nomadic-labs.com> *)

(* Permission is hereby granted, free of charge, to any person obtaining a *)
(* copy of this software and associated documentation files (the "Software"), *)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense, *)
(* and/or sell copies of the Software, and to permit persons to whom the *)
(* Software is furnished to do so, subject to the following conditions: *)

(* The above copyright notice and this permission notice shall be included *)
(* in all copies or substantial portions of the Software. *)

(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER *)
(* DEALINGS IN THE SOFTWARE. *)

(* This file contains invariant proofs for the multisig managed state contract.
   That is, we prove that some properties of the storage of the contract are
   preserved by all executions. Checking that these properties hold for the
   initial storage (the storage of the contract at origination) should still
   be done. *)

Require Import ZArith Lia String Uint63.
From Michocoq Require Import syntax.
Import Michocoq.error.Notations.
Require correctness_theorem.
Import specification.

Definition storage_ty := correctness_theorem.storage_ty.
Definition parameter_ty := correctness_theorem.parameter_ty.
Definition self_ty := correctness_theorem.self_ty.
Definition code := correctness_theorem.code.
Definition action_ty : type := lambda unit (list operation).
Definition approvals_ty : type := set address.
Definition proposal_ty : type := pair timestamp (pair action_ty approvals_ty).
Definition op_id_ty : comparable_type := nat.

Require Export contract_semantics.

Definition invariant (P : data storage_ty -> Prop) :=
  exists min_fuel,
  forall arg : data (entrypoints.entrypoint_tree_to_type parameter_ty),
  forall old_storage new_storage : data storage_ty,
  forall emmited_operations : data (list operation),
  forall env fuel,
    fuel >= min_fuel ->
    P old_storage ->
    eval_seq env code fuel ((arg, old_storage), tt) = error.Return ((emmited_operations, new_storage), tt) ->
    P new_storage.

Definition authentication
           (param : data (entrypoints.entrypoint_tree_to_type parameter_ty))
           (sender : data address)
           (owner : data address)
           (signers : data (set address)) : Prop :=
  match read_parameter param with
  | Set_metadata_uri _ => sender = owner
  | Default => Logic.True
  | _ =>
    mem_v _ _ (Mem_variant_set address) sender signers = true
  end.

Definition precondition
           (param : data (entrypoints.entrypoint_tree_to_type parameter_ty))
           (env : @proto_env self_ty)
           (storage : data storage_ty) fuel : Prop :=
  let storage := read_storage storage in
  authentication param env.(sender) storage.(owner) storage.(signers) /\
  match read_parameter param with
  | Set_metadata_uri _ | Default => Logic.True
  | _ => env.(amount) = 0%uint63
  end /\
  match read_parameter param with
  | Set_metadata_uri arg => Logic.True
  | Default => (storage.(threshold) <= N.of_nat (set.size storage.(signers)))%N
  | Propose action duration =>
    (duration > 0)%Z /\
    map.mem (storage.(last_op_id) + 1)%N storage.(pending_ops) = false
  | Approve arg =>
    exists proposal : data proposal_ty,
      get_v _ _ _ (Get_variant_bigmap op_id_ty proposal_ty) arg storage.(pending_ops) = Some proposal /\
      let '(timeout, (_action, approvals)) := proposal in
      mem_v _ _ (Mem_variant_set address) env.(sender) approvals = false /\
      (now env < timeout)%Z
  | Execute arg =>
    exists proposal : data proposal_ty,
      get_v _ _ _ (Get_variant_bigmap op_id_ty proposal_ty) arg storage.(pending_ops) = Some proposal /\
      let '(_timeout, (build_lam _ _ _ action, approvals)) := proposal in
      (N.of_nat (size_v _ (Size_variant_set address) approvals) >= storage.(threshold))%N /\
      match eval_seq (no_self env) action fuel (tt, tt) with
      | error.Failed _ _ => Logic.False
      | error.Return (_, tt) => Logic.True
      end
  | Archive_expired_operation arg =>
    exists proposal : data proposal_ty,
      get_v _ _ _ (Get_variant_bigmap op_id_ty proposal_ty) arg storage.(pending_ops) = Some proposal /\
      let '(timeout, (_action, approvals)) := proposal in
      (N.of_nat (size_v _ (Size_variant_set address) approvals) < storage.(threshold))%N /\
      (timeout < now env)%Z
  end.

Definition returned_operations
           (param : data (entrypoints.entrypoint_tree_to_type parameter_ty))
           (env : @proto_env self_ty)
           (storage : data storage_ty) fuel : data (list operation) :=
  let storage := read_storage storage in
  match read_parameter param with
  | Execute arg =>
    match get_v _ _ _ (Get_variant_bigmap op_id_ty proposal_ty) arg storage.(pending_ops) with
    | Some (_timeout, (build_lam _ _ _ action, _approvals)) =>
      match (eval_seq (no_self env) action fuel (tt, tt)) with
      | error.Return (operations, tt) => operations
      | error.Failed _ _ => nil
      end
    | None => nil
    end
  | _ => nil
  end.

Definition returned_pending_ops
           (param : data (entrypoints.entrypoint_tree_to_type parameter_ty))
           (env : @proto_env self_ty)
           (last_op_id : data op_id_ty)
           (old_pending_ops : data pending_ops_ty) : data pending_ops_ty :=
  match read_parameter param with
  | Propose action duration =>
    let no_approvals : data approvals_ty := set.empty in
    let approvals : data approvals_ty :=
        update_v _ _ _ (Update_variant_set address) env.(sender) true no_approvals
    in
    let proposal : data proposal_ty :=
        ((env.(now) + duration)%Z, (action, approvals))
    in
    let new_op_id := (last_op_id + 1)%N in
    update_v _ _ _ (Update_variant_bigmap op_id_ty proposal_ty)
             new_op_id
             (Some proposal)
             old_pending_ops
  | Approve arg =>
    match get_v _ _ _ (Get_variant_bigmap op_id_ty proposal_ty) arg old_pending_ops with
    | Some (timeout, (action, old_approvals)) =>
      let new_approvals : data approvals_ty :=
          update_v _ _ _ (Update_variant_set address) env.(sender) true old_approvals
      in
      let new_proposal : data proposal_ty := (timeout, (action, new_approvals)) in
      update_v _ _ _ (Update_variant_bigmap op_id_ty proposal_ty) arg (Some new_proposal) old_pending_ops
    | None => old_pending_ops
    end
  | Execute arg
  | Archive_expired_operation arg =>
    map.remove (comparable.lt_trans nat) arg old_pending_ops
  | _ => old_pending_ops
  end.

Definition returned_storage
           (param : data (entrypoints.entrypoint_tree_to_type parameter_ty))
           (env : @proto_env self_ty)
           (old_storage : data storage_ty) : data storage_ty :=
  let old_storage := read_storage old_storage in
  let new_owner := old_storage.(owner) in
  let new_signers := old_storage.(signers) in
  let new_threshold := old_storage.(threshold) in
  let new_last_op_id :=
      match read_parameter param with
      | Propose _ _ => (old_storage.(last_op_id) + 1)%N
      | _ => old_storage.(last_op_id)
      end
  in
  let new_pending_ops :=
      returned_pending_ops param env old_storage.(last_op_id) old_storage.(pending_ops)
  in
  let new_metadata :=
      match read_parameter param with
      | Set_metadata_uri arg =>
        map.update (comparable.compare_eq_iff string) (comparable.lt_trans string) (comparable.gt_trans string) ""%string (Some arg) old_storage.(metadata)
      | _ => old_storage.(metadata)
      end
  in
  (new_owner, (new_signers, (new_threshold, (new_last_op_id, (new_pending_ops, new_metadata))))).

Lemma and_true A : (A /\ Logic.True) <-> A.
Proof.
  intuition.
Qed.

(* Ltac rewrite_pair *)

Lemma and_pair_eq_data_aux (op : Set)
      (A B : type) (a c : data_aux op A) (b d : data_aux op B) :
  @eq (data_aux op (pair A B)) (a, b) (c, d) <-> a = c /\ b = d.
Proof.
  apply base.and_pair_eq.
Qed.

Lemma and_pair_eq_data (A B : type) (a c : data A) (b d : data B) :
  @eq (data (pair A B)) (a, b) (c, d) <-> a = c /\ b = d.
Proof.
  apply base.and_pair_eq.
Qed.

Lemma assert_iff (P A B : Prop) :
  (A -> P) -> (B -> P) -> (P -> (A <-> B)) -> (A <-> B).
Proof.
  intuition.
Qed.

Lemma multisig_managed_state_correct_bis
      (fuel : Datatypes.nat)
      (env : proto_env)
      (new_storage : data correctness_theorem.storage_ty)
      (emmited_operations : data (list operation))
      (arg : data
                  (entrypoints.entrypoint_tree_to_type
                     correctness_theorem.parameter_ty))
      (old_storage : data correctness_theorem.storage_ty) :
  fuel >= 2 ->
  eval_seq env correctness_theorem.code (4 + fuel)
           (arg, old_storage, tt) =
  error.Return (emmited_operations, new_storage, tt) <->
  (precondition arg env old_storage fuel /\
   emmited_operations = returned_operations arg env old_storage fuel /\
   new_storage = returned_storage arg env old_storage).
Proof.
  intro Hfuel.
  unfold correctness_theorem.storage_ty, parsing.storage_ty, data.
  simpl data_aux.
  rewrite correctness_theorem.multisig_managed_state_correct; [|exact Hfuel].
  unfold contract_specification.
  unfold precondition, authentication, returned_operations, returned_storage.
  destruct new_storage as
      (new_owner, (new_signers, (new_threshold, (new_last_op_id, (new_pending_ops, new_metadata))))).
  destruct arg as [[arg|[]]|[[(action, duration)|arg]|[arg|arg]]]; simpl.
  - unfold set_metadata_uri_spec; simpl.
    unfold update_metada_bigmap; simpl.
    rewrite and_true.
    rewrite and_true.
    repeat rewrite base.and_pair_eq.
    reflexivity.
  - unfold default_spec; simpl.
    repeat rewrite base.and_pair_eq.
    destruct old_storage as
        (old_owner, (old_signers, (old_threshold, (old_last_op_id, (old_pending_ops, old_metadata))))).
    simpl.
    split.
    + intros (He, (Hs, Ht)).
      injection Hs.
      intuition.
    + unfold returned_pending_ops; simpl.
      intuition.
      subst.
      reflexivity.
  - unfold propose_spec; simpl.
    progress unfold storage_invariant_spec; simpl.
    repeat rewrite base.and_pair_eq.
    intuition.
  - unfold approve_spec; simpl.
    progress unfold storage_invariant_spec; simpl.
    repeat rewrite and_assoc.
    progress repeat rewrite base.ex_and_comm_right.
    progress repeat rewrite base.ex_and_comm_left.
    apply base.forall_ex.
    intros (timeout, (action, old_approvals)).
    repeat rewrite base.and_pair_eq.
    intuition.
    + unfold returned_pending_ops; simpl.
      rewrite H6.
      assumption.
    + unfold returned_pending_ops in H9; simpl in H9.
      rewrite H2 in H9.
      assumption.
  - unfold execute_spec; simpl.
    progress unfold storage_invariant_spec; simpl.
    repeat rewrite and_assoc.
    progress repeat rewrite base.ex_and_comm_right.
    progress repeat rewrite base.ex_and_comm_left.
    apply base.forall_ex.
    intros (timeout, ((tff, action), old_approvals)).
    repeat rewrite base.and_pair_eq.
    unfold self_ty.
    apply (assert_iff (@map.get _ (prod Z
                       (prod (data_lam (Comparable_type unit) (list operation))
                          (set.set comparable.address_constant
                             comparable.address_compare))) N.compare arg (pending_ops (read_storage old_storage)) = Some (timeout, (build_lam unit (list operation) tff action, old_approvals))));
      [solve [intuition]|solve [intuition]|intro Hget];
      rewrite Hget.
    destruct eval_seq as [err|(ops, [])]; solve [intuition].
  - unfold archive_expired_operation_spec; simpl.
    progress unfold storage_invariant_spec; simpl.
    repeat rewrite and_assoc.
    progress repeat rewrite base.ex_and_comm_right.
    progress repeat rewrite base.ex_and_comm_left.
    apply base.forall_ex.
    intros (timeout, ((tff, action), old_approvals)).
    repeat rewrite base.and_pair_eq.
    intuition.
Qed.

Lemma multisig_managed_state_precondition_correct
      (fuel : Datatypes.nat)
      (env : proto_env)
      (arg : data
                  (entrypoints.entrypoint_tree_to_type
                     correctness_theorem.parameter_ty))
      (old_storage : data correctness_theorem.storage_ty) :
  fuel >= 2 ->
  precondition arg env old_storage fuel <->
  error.success
    (eval_seq env correctness_theorem.code (4 + fuel)
              (arg, old_storage, tt)) = true.
Proof.
  intro Hfuel.
  rewrite <- base.IT_eq_iff.
  split; intro H.
  - assert
    (precondition arg env old_storage fuel /\
     returned_operations arg env old_storage fuel = returned_operations arg env old_storage fuel /\
     returned_storage arg env old_storage = returned_storage arg env old_storage)
    as H2 by solve [intuition].
    rewrite <- multisig_managed_state_correct_bis in H2; [|assumption].
    apply error.success_eq_return in H2.
    exact H2.
  - apply error.success_eq_return_rev in H.
    destruct H as (((ops, new_storage), []), H).
    rewrite multisig_managed_state_correct_bis in H; [|assumption].
    solve [intuition].
Qed.

(* The owner field in the storage cannot change. *)
Lemma owner_invariant o :
  invariant (fun storage => (read_storage storage).(owner) = o).
Proof.
  exists 6.
  intros arg old_storage new_storage emmited_operations env fuel Hfuel Hold.
  unfold ">=" in Hfuel.
  change 6 with (4 + 2) in Hfuel.
  more_fuel_add.
  rewrite multisig_managed_state_correct_bis; [|exact Hfuel].
  intros (_, (_, H)).
  subst new_storage.
  unfold returned_storage.
  simpl.
  exact Hold.
Qed.

(* The signers field in the storage cannot change. *)
Lemma signers_invariant s :
  invariant (fun storage => (read_storage storage).(signers) = s).
Proof.
  exists 6.
  intros arg old_storage new_storage emmited_operations env fuel Hfuel Hold.
  unfold ">=" in Hfuel.
  change 6 with (4 + 2) in Hfuel.
  more_fuel_add.
  rewrite multisig_managed_state_correct_bis; [|exact Hfuel].
  intros (_, (_, H)).
  subst new_storage.
  unfold returned_storage.
  simpl.
  exact Hold.
Qed.

(* The threshold field in the storage cannot change. *)
Lemma threshold_invariant t :
  invariant (fun storage => (read_storage storage).(threshold) = t).
Proof.
  exists 6.
  intros arg old_storage new_storage emmited_operations env fuel Hfuel Hold.
  unfold ">=" in Hfuel.
  change 6 with (4 + 2) in Hfuel.
  more_fuel_add.
  rewrite multisig_managed_state_correct_bis; [|exact Hfuel].
  intros (_, (_, H)).
  subst new_storage.
  unfold returned_storage.
  simpl.
  exact Hold.
Qed.

(* The last_op_id field in the storage is non-decreasing. *)
Lemma last_op_id_non_decreasing_invariant lowest_id :
  invariant (fun storage => (read_storage storage).(last_op_id) >= lowest_id)%N.
Proof.
  exists 6.
  intros arg old_storage new_storage emmited_operations env fuel Hfuel Hold.
  unfold ">=" in Hfuel.
  change 6 with (4 + 2) in Hfuel.
  more_fuel_add.
  rewrite multisig_managed_state_correct_bis; [|exact Hfuel].
  intros (_, (_, H)).
  subst new_storage.
  unfold returned_storage.
  simpl.
  destruct arg as [[arg|[]]|[[(action, duration)|arg]|[arg|arg]]]; simpl; lia.
Qed.

Lemma mem_insert_neq (A B : Set) (compare : A -> A -> comparison) (compare_eq_iff : forall a b : A, compare a b = Eq <-> a = b)
  (lt_trans :RelationClasses.Transitive (fun a b : A => compare a b = Lt))
  (gt_trans : RelationClasses.Transitive (fun a b : A => compare a b = Gt)) (a b : A) (v : B) (m : map.map A B compare) :
  a <> b -> map.mem a (map.insert compare_eq_iff gt_trans b v m) = map.mem a m.
Proof.
  intro Hneq.
  do 2 rewrite map.mem_get.
  f_equal.
  apply map.get_insert_neq; assumption.
Qed.

Lemma mem_remove_neq (A B : Set) (compare : A -> A -> comparison) (compare_eq_iff : forall a b : A, compare a b = Eq <-> a = b)
  (lt_trans :RelationClasses.Transitive (fun a b : A => compare a b = Lt))
  (a b : A) (m : map.map A B compare) :
  a <> b -> map.mem a (map.remove lt_trans b m) = map.mem a m.
Proof.
  intro Hneq.
  do 2 rewrite map.mem_get.
  f_equal.
  apply map.get_remove_neq; assumption.
Qed.

Lemma mem_get_some (A B : Set) (compare : A -> A -> comparison) (a : A) (b : B) (m : map.map A B compare) :
  map.get a m = Some b -> map.mem a m = true.
Proof.
  intro H.
  rewrite map.mem_get.
  rewrite H.
  reflexivity.
Qed.

Definition last_op_id_is_really_the_last (storage : data storage_ty) :=
  let storage := read_storage storage in
  let last_op_id := storage.(last_op_id) in
  let pending_ops := storage.(pending_ops) in
  forall id, (id > last_op_id)%N ->
             mem_v _ _ (Mem_variant_bigmap op_id_ty proposal_ty) id pending_ops = false.

(* There is no proposal with id > last_op_id *)
Lemma last_op_id_is_really_the_last_invariant :
  invariant last_op_id_is_really_the_last.
Proof.
  exists 6.
  intros arg old_storage new_storage emmited_operations env fuel Hfuel Hold.
  unfold ">=" in Hfuel.
  change 6 with (4 + 2) in Hfuel.
  more_fuel_add.
  rewrite multisig_managed_state_correct_bis; [|exact Hfuel].
  intros (Hprecond, (_, H)).
  subst new_storage.
  unfold returned_storage.
  simpl in *.
  intros id Hid.
  destruct arg as [[arg|[]]|[[(action, duration)|arg]|[arg|arg]]]; simpl in *;
    specialize (Hold id);
    try specialize (Hold Hid);
    try exact Hold.
  - assert (id > last_op_id (read_storage old_storage))%N as Hid2 by lia.
    specialize (Hold Hid2).
    unfold returned_pending_ops; simpl.
    rewrite mem_insert_neq.
    + assumption.
    + refine _.
    + lia.
  - unfold precondition in Hprecond.
    simpl in Hprecond.
    destruct Hprecond as (_, (_, ((timeout, (action, old_approvals)), (Heq, _)))).
    unfold returned_pending_ops; simpl.
    rewrite Heq.
    rewrite mem_insert_neq.
    + assumption.
    + refine _.
    + apply mem_get_some in Heq.
      intro; subst id.
      enough (true = false) by discriminate.
      rewrite <- Heq.
      rewrite <- Hold.
      reflexivity.
  - unfold precondition in Hprecond.
    simpl in Hprecond.
    destruct Hprecond as (_, (_, ((timeout, (action, old_approvals)), (Heq, _)))).
    unfold returned_pending_ops; simpl.
    rewrite mem_remove_neq.
    + assumption.
    + apply N.compare_eq_iff.
    + apply mem_get_some in Heq.
      intro; subst id.
      enough (true = false) by discriminate.
      rewrite <- Heq.
      rewrite <- Hold.
      reflexivity.
  - unfold precondition in Hprecond.
    simpl in Hprecond.
    destruct Hprecond as (_, (_, ((timeout, (action, old_approvals)), (Heq, _)))).
    unfold returned_pending_ops; simpl.
    rewrite mem_remove_neq.
    + assumption.
    + apply N.compare_eq_iff.
    + apply mem_get_some in Heq.
      intro; subst id.
      enough (true = false) by discriminate.
      rewrite <- Heq.
      rewrite <- Hold.
      reflexivity.
Qed.

Definition get_remove_neq :=
  map.get_remove_neq
    (comparable.comparable_data nat)
    (data proposal_ty)
    _
    (comparable.compare_eq_iff nat)
    (comparable.lt_trans nat).

Definition get_insert_neq :=
  map.get_insert_neq
    (comparable.comparable_data nat)
    (data proposal_ty)
    _
    (comparable.compare_eq_iff nat)
    (comparable.lt_trans nat)
    (comparable.gt_trans nat).

Definition get_remove_eq :=
  map.get_remove_eq
    (comparable.comparable_data nat)
    (data proposal_ty)
    _
    (comparable.compare_eq_iff nat)
    (comparable.lt_trans nat).

(* Archiving an expired proposal commutes with any call so it never
   prevents from doing anything with the contract. *)
(* First an aux lemma regarding the action on the storage *)
Lemma archive_commutes_storage
  (arg : data (entrypoints.entrypoint_tree_to_type parameter_ty))
  (id_to_archive : data op_id_ty)
  (old_storage : data storage_ty) fuel
  env1 env2 :
  (now env1 <= now env2)%Z ->
  last_op_id_is_really_the_last old_storage ->
  precondition (inr (inr (inr id_to_archive))) env1 old_storage fuel ->
  precondition arg env2 old_storage fuel ->
  returned_storage arg env2 (returned_storage (inr (inr (inr id_to_archive))) env1 old_storage)
  =
  returned_storage (inr (inr (inr id_to_archive))) env1 (returned_storage arg env2 old_storage).
Proof.
  intros Hnow Hlast_op_id Hprecond_archive Hprecond.
  destruct old_storage as
      (old_owner, (old_signers, (old_threshold, (old_last_op_id, (old_pending_ops, old_metadata))))).
  unfold returned_storage; simpl.
  do 5 f_equal.
  apply (map.extensionality N.compare_eq_iff _ (comparable.gt_trans nat)).
  intro id.
  destruct arg as [[arg|[]]|[[(action, duration)|arg]|[arg|arg]]];
    unfold returned_storage, returned_pending_ops; simpl in *.
  - (* Set_metadata_uri *)
    reflexivity.
  - (* Default *)
    reflexivity.
  - (* Propose *)
    unfold precondition in Hprecond_archive; simpl in Hprecond_archive.
    destruct Hprecond_archive as (_, (_, (expired_proposal, (Hget_expired, _)))).
    assert (id_to_archive <> old_last_op_id + 1)%N as Hid_to_archive_is_not_last_p1.
    + unfold last_op_id_is_really_the_last in Hlast_op_id.
      specialize (Hlast_op_id (old_last_op_id + 1)%N).
      simpl in Hlast_op_id.
      assert (old_last_op_id + 1 > old_last_op_id)%N as H by lia.
      specialize (Hlast_op_id H).
      apply mem_get_some in Hget_expired.
      intro; subst.
      enough (true = false) by discriminate.
      rewrite <- Hget_expired.
      rewrite <- Hlast_op_id.
      reflexivity.
    + destruct (N.eq_decidable id (old_last_op_id + 1)%N).
      * subst id.
        rewrite map.get_insert_eq.
        rewrite get_remove_neq.
        -- rewrite map.get_insert_eq.
           reflexivity.
        -- intro; congruence.
      * rewrite get_insert_neq; [|assumption].
        destruct (N.eq_decidable id id_to_archive).
        -- subst id_to_archive.
           rewrite get_remove_eq.
           rewrite get_remove_eq.
           reflexivity.
        -- rewrite get_remove_neq; [|assumption].
           rewrite get_remove_neq; [|assumption].
           rewrite get_insert_neq; [|assumption].
           reflexivity.
  - (* Approve *)
    unfold precondition in Hprecond_archive; simpl in Hprecond_archive.
    destruct Hprecond_archive as (_, (_, ((past_timeout, (expired_action, expired_approvals)), (Hget_expired, (Hexpired_approvals, Hexpired))))).
    unfold precondition in Hprecond; simpl in Hprecond.
    destruct Hprecond as (_, (_, ((timeout, (action, approvals)), (Hget, (Happrovals, Htimeout))))).
    assert (arg <> id_to_archive).
    + intro; subst arg.
      rewrite Hget_expired in Hget.
      injection Hget.
      intros; subst.
      lia.
    + rewrite get_remove_neq; [|assumption].
      rewrite Hget.
      unfold data, proposal_ty.
      simpl.
      rewrite Hget.
      destruct (N.eq_decidable id arg); destruct (N.eq_decidable id id_to_archive).
      * congruence.
      * subst.
        rewrite map.get_insert_eq.
        rewrite get_remove_neq; [|assumption].
        rewrite map.get_insert_eq.
        reflexivity.
      * subst.
        rewrite get_insert_neq; [|assumption].
        rewrite get_remove_eq.
        rewrite get_remove_eq.
        reflexivity.
      * rewrite get_insert_neq; [|assumption].
        rewrite get_remove_neq; [|assumption].
        rewrite get_remove_neq; [|assumption].
        rewrite get_insert_neq; [|assumption].
        reflexivity.
  - (* Execute *)
    destruct (N.eq_decidable id arg); destruct (N.eq_decidable id id_to_archive).
    + subst; reflexivity.
    + subst.
      rewrite get_remove_eq.
      rewrite get_remove_neq; [|assumption].
      rewrite get_remove_eq.
      reflexivity.
    + subst.
      rewrite get_remove_neq; [|assumption].
      rewrite get_remove_eq.
      rewrite get_remove_eq.
      reflexivity.
    + rewrite get_remove_neq; [|assumption].
      rewrite get_remove_neq; [|assumption].
      rewrite get_remove_neq; [|assumption].
      rewrite get_remove_neq; [|assumption].
      reflexivity.
  - (* Archive *)
    destruct (N.eq_decidable id arg); destruct (N.eq_decidable id id_to_archive).
    + subst; reflexivity.
    + subst.
      rewrite get_remove_eq.
      rewrite get_remove_neq; [|assumption].
      rewrite get_remove_eq.
      reflexivity.
    + subst.
      rewrite get_remove_neq; [|assumption].
      rewrite get_remove_eq.
      rewrite get_remove_eq.
      reflexivity.
    + rewrite get_remove_neq; [|assumption].
      rewrite get_remove_neq; [|assumption].
      rewrite get_remove_neq; [|assumption].
      rewrite get_remove_neq; [|assumption].
      reflexivity.
Qed.

(* Archiving an expired proposal commutes with any call so it never
   prevents from doing anything with the contract. *)
Lemma archive_commutes
      (arg : data (entrypoints.entrypoint_tree_to_type parameter_ty))
      (id_to_archive : data op_id_ty)
      (old_storage new_storage : data storage_ty)
      (emmited_operations : data (list operation))
      env1 env2 fuel :
    fuel >= 6 ->
    (now env1 <= now env2)%Z ->
    last_op_id_is_really_the_last old_storage ->
    (let! ((ops1, mid_storage), tt) :=
        eval_seq env1 code fuel ((inr (inr (inr id_to_archive)), old_storage), tt)
     in
     let! ((ops2, final_storage), tt) :=
        eval_seq env2 code fuel ((arg, mid_storage), tt)
     in
     error.Return (ops1, ops2, final_storage))
    = error.Return (nil, emmited_operations, new_storage)
    ->
    (let! ((ops1, mid_storage), tt) :=
        eval_seq env2 code fuel ((arg, old_storage), tt)
     in
     let! ((ops2, final_storage), tt) :=
        eval_seq env1 code fuel ((inr (inr (inr id_to_archive)), mid_storage), tt)
     in
     error.Return (ops1, ops2, final_storage))
    = error.Return (emmited_operations, nil, new_storage).
Proof.
  intros Hfuel Hnow Hlast_op_id.
  unfold ">=" in Hfuel.
  change 6 with (4 + 2) in Hfuel.
  more_fuel_add.
  intro Hold_mid.
  apply error.bind_eq_return in Hold_mid.
  destruct Hold_mid as (((ops1, mid_storage), []), (Hold, Hmid)).
  apply error.bind_eq_return in Hmid.
  destruct Hmid as (((ops2, final_storage), []), (Hmid, He)).
  apply error.unreturn in He.
  injection He; clear He.
  intros; subst.
  rewrite multisig_managed_state_correct_bis in Hold; [|exact Hfuel].
  rewrite multisig_managed_state_correct_bis in Hmid; [|exact Hfuel].
  destruct Hold as (Hold_precond, (_, Hold_storage)).
  destruct Hmid as (Hmid_precond, (Hmid_ops, Hmid_storage)).
  apply error.bind_eq_return.
  exists (returned_operations arg env2 old_storage fuel, returned_storage arg env2 old_storage, tt).
  rewrite multisig_managed_state_correct_bis; [|exact Hfuel].
  subst mid_storage new_storage.
  assert (precondition arg env2 old_storage fuel) as Hprecond2.
  - unfold precondition.
    unfold returned_storage in Hmid_precond.
    simpl in Hmid_precond.
    unfold precondition in Hmid_precond.
    unfold authentication in *.
    destruct Hmid_precond as (Hauth, (Hamount, Hmid_precond)).
    destruct arg as [[arg|[]]|[[(action, duration)|arg]|[arg|arg]]];
      simpl in *; (split; [assumption|split; [assumption|]]); try assumption.
    + (* Propose *)
      destruct Hmid_precond as (Hduration, Hmid_precond).
      split; [exact Hduration|].
      rewrite <- Hmid_precond.
      symmetry.
      apply mem_remove_neq; [apply N.compare_eq_iff|].
      intro; subst.
      unfold precondition in Hold_precond.
      simpl in Hold_precond.
      clear Hmid_precond.
      unfold last_op_id_is_really_the_last in Hlast_op_id.
      assert (last_op_id (read_storage old_storage) + 1 >
              last_op_id (read_storage old_storage))%N as H by lia.
      specialize (Hlast_op_id _ H).
      simpl in Hlast_op_id.
      rewrite map.mem_get_false in Hlast_op_id.
      rewrite Hlast_op_id in Hold_precond.
      destruct Hold_precond as (_, (_, (proposal, (H2, _)))).
      discriminate.
    + (* Approve *)
      destruct Hmid_precond as (proposal, (Hget, H)).
      exists proposal.
      destruct proposal as (timeout, (action, approvals)).
      split; [|assumption].
      unfold returned_pending_ops in Hget; simpl in Hget.
      rewrite get_remove_neq in Hget; [exact Hget|].
      intro He.
      subst id_to_archive.
      rewrite get_remove_eq in Hget.
      discriminate.
    + (* Execute *)
      destruct Hmid_precond as (proposal, (Hget, H)).
      exists proposal.
      destruct proposal as (timeout, ((tff, action), approvals)).
      split; [|assumption].
      unfold returned_pending_ops in Hget; simpl in Hget.
      rewrite get_remove_neq in Hget; [exact Hget|].
      intro He.
      subst id_to_archive.
      rewrite get_remove_eq in Hget.
      discriminate.
    + (* Archive *)
      destruct Hmid_precond as (proposal, (Hget, H)).
      exists proposal.
      destruct proposal as (timeout, (action, approvals)).
      split; [|assumption].
      unfold returned_pending_ops in Hget; simpl in Hget.
      rewrite get_remove_neq in Hget; [exact Hget|].
      intro He.
      subst id_to_archive.
      rewrite get_remove_eq in Hget.
      discriminate.
  - rewrite archive_commutes_storage with (fuel := fuel); try assumption.
    split.
    + split; [assumption|split; reflexivity].
    + apply error.bind_eq_return.
      exists (returned_operations (inr (inr (inr id_to_archive))) env1 (returned_storage arg env2 old_storage) fuel, returned_storage (inr (inr (inr id_to_archive))) env1 (returned_storage arg env2 old_storage), tt).
      rewrite multisig_managed_state_correct_bis; [|exact Hfuel].
      split.
      * split; [|split; reflexivity].
        unfold returned_storage in Hmid_precond; simpl in *.
        destruct old_storage as
            (old_owner, (old_signers, (old_threshold, (old_last_op_id, (old_pending_ops, old_metadata))))).
        simpl in *.
        destruct Hold_precond as (Hold_auth, Hold_precond).
        split; [assumption|].
        simpl in *.
        destruct Hold_precond as (Hamount, (expired_proposal, (Hget_expired, Hold_precond))).
        split; [exact Hamount|].
        exists expired_proposal.
        split; [|exact Hold_precond].
        destruct arg as [[arg|[]]|[[(action, duration)|arg]|[arg|arg]]];
          simpl in *.
        -- assumption.
        -- assumption.
        -- unfold returned_pending_ops; simpl.
           rewrite get_insert_neq; [assumption|].
           specialize (Hlast_op_id id_to_archive).
           intro; subst id_to_archive.
           simpl in Hlast_op_id.
           assert (old_last_op_id + 1 > old_last_op_id)%N as H by lia.
           specialize (Hlast_op_id H).
           apply mem_get_some in Hget_expired.
           enough (true = false) by discriminate.
           rewrite <- Hget_expired.
           rewrite <- Hlast_op_id.
           reflexivity.
        -- unfold precondition in Hmid_precond; simpl in Hmid_precond.
           destruct Hmid_precond as (_, (_, (proposal, (Hget, Hmid_precond)))).
           assert (arg <> id_to_archive) as Harg.
           ++ intro; subst.
              unfold returned_pending_ops in Hget; simpl in Hget.
              rewrite get_remove_eq in Hget.
              discriminate.
           ++ unfold returned_pending_ops in Hget; simpl in Hget.
              rewrite get_remove_neq in Hget; [|assumption].
              unfold returned_pending_ops; simpl.
              unfold data, proposal_ty in Hget.
              simpl in Hget.
              rewrite Hget.
              destruct proposal as (timeout, (action, old_approvals)).
              rewrite get_insert_neq; intuition congruence.
        -- unfold precondition in Hmid_precond; simpl in Hmid_precond.
           destruct Hmid_precond as (_, (_, (proposal, (Hget, Hmid_precond)))).
           assert (arg <> id_to_archive) as Harg.
           ++ intro; subst.
              unfold returned_pending_ops in Hget; simpl in Hget.
              rewrite get_remove_eq in Hget.
              discriminate.
           ++ unfold returned_pending_ops in Hget; simpl in Hget.
              rewrite get_remove_neq in Hget; [|assumption].
              unfold returned_pending_ops; simpl.
              rewrite get_remove_neq; [assumption|congruence].
        -- unfold precondition in Hmid_precond; simpl in Hmid_precond.
           destruct Hmid_precond as (_, (_, (proposal, (Hget, Hmid_precond)))).
           assert (arg <> id_to_archive) as Harg.
           ++ intro; subst.
              unfold returned_pending_ops in Hget; simpl in Hget.
              rewrite map.get_remove_eq in Hget;
                [discriminate|apply N.compare_eq_iff].
           ++ unfold returned_pending_ops in Hget; simpl in Hget.
              rewrite get_remove_neq in Hget; [|assumption].
              unfold returned_pending_ops; simpl.
              rewrite get_remove_neq; [assumption|congruence].
      * f_equal.
        subst emmited_operations.
        f_equal.
        unfold returned_operations at 2; simpl.
        f_equal.
        unfold returned_storage; simpl.
        destruct old_storage as
            (old_owner, (old_signers, (old_threshold, (old_last_op_id, (old_pending_ops, old_metadata))))).
        simpl.
        destruct arg as [[arg|[]]|[[(action, duration)|arg]|[arg|arg]]];
          unfold returned_operations; simpl; try reflexivity.
        unfold returned_pending_ops; simpl.
        rewrite get_remove_neq; [reflexivity|].
        intro; subst.
        unfold precondition in Hmid_precond; simpl in Hmid_precond.
        destruct Hmid_precond as (_, (_, (proposal, (Habs, _)))).
        unfold returned_pending_ops in Habs; simpl in Habs.
        rewrite get_remove_eq in Habs.
        discriminate.
Qed.

Definition invariant_with_ghost_code
           (ghost_type : Set)
           (ghost_eval :
              data (entrypoints.entrypoint_tree_to_type parameter_ty) ->
              @proto_env self_ty ->
              data storage_ty ->
              ghost_type ->
              ghost_type)
           (P : data storage_ty -> ghost_type -> Prop) :=
  exists min_fuel,
  forall arg : data (entrypoints.entrypoint_tree_to_type parameter_ty),
  forall old_storage new_storage : data storage_ty,
  forall old_ghost : ghost_type,
  forall emmited_operations : data (list operation),
  forall env fuel,
    fuel >= min_fuel ->
    P old_storage old_ghost ->
    eval_seq env code fuel ((arg, old_storage), tt) = error.Return ((emmited_operations, new_storage), tt) ->
    P new_storage (ghost_eval arg env old_storage old_ghost).

Section no_identifier_recycling_inv.

  (* Identifiers are never recycled: if at two points in time in the execution
     of the contract the same idetifier is used in the pending_ops table then
     it is associated to the same timestamp and action (and the approvals
     of the oldest version are included in the approvals of the newest
     version). We cannot state this without mentioning chains of execution
     but we can present this property as an invariant by extending the
     storage with a ghost copy of the pending ops table on which operations
     are never removed. We state the invariant as "pending_ops is included
     in the ghost map". *)
  Definition ghost_type := data (big_map op_id_ty proposal_ty).

  (* Same as returned_pending_ops but without deletion. *)
  Definition ghost_eval
             (param : data (entrypoints.entrypoint_tree_to_type parameter_ty))
             (env : @proto_env self_ty)
             (old_storage : data storage_ty)
             (old_ghost_map : ghost_type) :=
    let old_storage := read_storage old_storage in
  match read_parameter param with
  | Propose action duration =>
    let no_approvals : data approvals_ty := set.empty in
    let approvals : data approvals_ty :=
        update_v _ _ _ (Update_variant_set address) env.(sender) true no_approvals
    in
    let proposal : data proposal_ty :=
        ((env.(now) + duration)%Z, (action, approvals))
    in
    let new_op_id := (old_storage.(last_op_id) + 1)%N in
    update_v _ _ _ (Update_variant_bigmap op_id_ty proposal_ty)
             new_op_id
             (Some proposal)
             old_ghost_map
  | Approve arg =>
    match get_v _ _ _ (Get_variant_bigmap op_id_ty proposal_ty) arg old_storage.(pending_ops) with
    | Some (timeout, (action, old_approvals)) =>
      let new_approvals : data approvals_ty :=
          update_v _ _ _ (Update_variant_set address) env.(sender) true old_approvals
      in
      let new_proposal : data proposal_ty := (timeout, (action, new_approvals)) in
      update_v _ _ _ (Update_variant_bigmap op_id_ty proposal_ty) arg (Some new_proposal) old_ghost_map
    | None => old_ghost_map
    end
  | _ => old_ghost_map
  end.

  Lemma no_identifier_recycling_inv :
    invariant_with_ghost_code
      ghost_type ghost_eval
      (fun storage ghost_map =>
         forall id,
         forall proposal : data proposal_ty,
           map.get id (read_storage storage).(pending_ops) = Some proposal ->
           map.get id ghost_map = Some proposal).
  Proof.
    exists 6.
    intros arg old_storage new_storage old_ghost emmited_operations env fuel
           Hfuel Hold_inv Heval id proposal Hget.
    unfold ">=" in Hfuel.
    change 6 with (4 + 2) in Hfuel.
    more_fuel_add.
    rewrite multisig_managed_state_correct_bis in Heval; [|exact Hfuel].
    destruct Heval as (Hprecond, (Hops, Hstorage)).
    specialize (Hold_inv id).
    subst.
    destruct arg as [[arg|[]]|[[(action, duration)|arg]|[arg|arg]]];
      unfold ghost_eval; unfold precondition in Hprecond;
        simpl in *;
        unfold returned_pending_ops in Hget;
        simpl in *;
        try solve [intuition].
    - (* Propose *)
      destruct (N.eq_decidable id ((read_storage old_storage).(last_op_id) + 1)%N).
      + subst id.
        rewrite map.get_insert_eq.
        rewrite map.get_insert_eq in Hget.
        exact Hget.
      + rewrite get_insert_neq; [|assumption].
        rewrite get_insert_neq in Hget; [|assumption].
        intuition.
    - (* Approve *)
      destruct (map.get arg) as [(timeout, (action, approvals))|] eqn:Hget_arg.
      + destruct (N.eq_decidable id arg).
        * subst id.
          rewrite map.get_insert_eq.
          rewrite map.get_insert_eq in Hget.
          exact Hget.
        * rewrite get_insert_neq; [|assumption].
          rewrite get_insert_neq in Hget; [|assumption].
          intuition.
      + intuition.
    - (* Execute *)
      destruct (N.eq_decidable id arg).
      + subst.
        rewrite get_remove_eq in Hget.
        discriminate.
      + rewrite get_remove_neq in Hget; [|assumption].
        intuition.
    - (* Archive *)
      destruct (N.eq_decidable id arg).
      + subst.
        rewrite get_remove_eq in Hget.
        discriminate.
      + rewrite get_remove_neq in Hget; [|assumption].
        intuition.
  Qed.

End no_identifier_recycling_inv.
