(* Open Source License *)
(* Copyright (c) 2022 Nomadic Labs. <contact@nomadic-labs.com> *)

(* Permission is hereby granted, free of charge, to any person obtaining a *)
(* copy of this software and associated documentation files (the "Software"), *)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense, *)
(* and/or sell copies of the Software, and to permit persons to whom the *)
(* Software is furnished to do so, subject to the following conditions: *)

(* The above copyright notice and this permission notice shall be included *)
(* in all copies or substantial portions of the Software. *)

(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER *)
(* DEALINGS IN THE SOFTWARE. *)

Require Import String.

Definition ep_set_metadata_uri : string := "DUP 2 ;
  SENDER ;
  COMPARE ;
  EQ ;
  NOT ;
  IF { PUSH string ""INVALID_CALLER"" ; FAILWITH } {} ;
  DUP 7 ;
  DUP 2 ;
  SOME ;
  PUSH string """" ;
  UPDATE ;
  DIP { DIG 6 ; DROP } ;
  DUG 6 ;
  DROP ;
  PAIR 6 ;
  DIG 1 ;
  PAIR".

Definition ep_default : string := "DROP ;
  DUP 3 ;
  DUP 3 ;
  SIZE ;
  COMPARE ;
  GE ;
  NOT ;
  IF { PUSH string ""FUNDING BLOCKED SINCE PARAMS ARE INVALID"" ; FAILWITH } {} ;
  PAIR 6 ;
  DIG 1 ;
  PAIR".

Definition ep_propose : string := "UNPAIR ;
  SWAP ;
  DUP 4 ;
  SENDER ;
  MEM ;
  NOT ;
  IF { PUSH string ""ONLY FOR SIGNERS"" ; FAILWITH } {} ;
  PUSH int 0 ;
  DUP 2 ;
  COMPARE ;
  GT ;
  NOT ;
  IF { PUSH string ""EXPIRED"" ; FAILWITH } {} ;
  PUSH mutez 0 ;
  AMOUNT ;
  COMPARE ;
  EQ ;
  NOT ;
  IF { PUSH string ""TO FUND CONTRACT, PLEASE USE THE DEFAULT ENTRYPOINT"" ; FAILWITH } {} ;
  PUSH nat 1 ;
  DUP 7 ;
  ADD ;
  DIP { DIG 5 ; DROP } ;
  DUG 5 ;
  DUP 7 ;
  DUP 7 ;
  MEM ;
  IF
    { PUSH string ""pending_ops"" ; PUSH string ""KEY_EXISTS"" ; PAIR ; FAILWITH }
    { DUP 7 ;
      EMPTY_SET address ;
      PUSH bool True ;
      SENDER ;
      UPDATE ;
      DUP 4 ;
      PAIR ;
      DUP 3 ;
      NOW ;
      ADD ;
      PAIR ;
      SOME ;
      DUP 8 ;
      UPDATE ;
      DIP { DIG 6 ; DROP } ;
      DUG 6 } ;
  DROP 2 ;
  PAIR 6 ;
  DIG 1 ;
  PAIR".

Definition ep_approve : string := "DUP 6 ;
  DUP 2 ;
  GET ;
  IF_NONE { PUSH string ""INVALID OP ID"" ; FAILWITH } {} ;
  DUP 4 ;
  SENDER ;
  MEM ;
  NOT ;
  IF { PUSH string ""ONLY FOR SIGNERS"" ; FAILWITH } {} ;
  DUP ;
  CDR ;
  CDR ;
  SENDER ;
  MEM ;
  IF { PUSH string ""ALREADY APPROVED"" ; FAILWITH } {} ;
  NOW ;
  DUP 2 ;
  CAR ;
  COMPARE ;
  GT ;
  NOT ;
  IF { PUSH string ""EXPIRED"" ; FAILWITH } {} ;
  PUSH mutez 0 ;
  AMOUNT ;
  COMPARE ;
  EQ ;
  NOT ;
  IF { PUSH string ""TO FUND CONTRACT, PLEASE USE THE DEFAULT ENTRYPOINT"" ; FAILWITH } {} ;
  DUP 7 ;
  DUP 8 ;
  DUP 4 ;
  GET ;
  IF_NONE { PUSH string ""pending_ops"" ; PUSH string ""ASSET_NOT_FOUND"" ; PAIR ; FAILWITH } {} ;
  UNPAIR ;
  SWAP ;
  UNPAIR ;
  SWAP ;
  DROP ;
  DUP 10 ;
  DUP 6 ;
  GET ;
  IF_NONE { PUSH string ""pending_ops"" ; PUSH string ""ASSET_NOT_FOUND"" ; PAIR ; FAILWITH } {} ;
  CDR ;
  CDR ;
  PUSH bool True ;
  SENDER ;
  UPDATE ;
  SWAP ;
  PAIR ;
  SWAP ;
  PAIR ;
  SOME ;
  DUP 4 ;
  UPDATE ;
  DIP { DIG 6 ; DROP } ;
  DUG 6 ;
  DROP 2 ;
  PAIR 6 ;
  DIG 1 ;
  PAIR".

Definition ep_execute : string := "DUP 6 ;
  DUP 2 ;
  GET ;
  IF_NONE { PUSH string ""INVALID OP ID"" ; FAILWITH } {} ;
  DUP 4 ;
  SENDER ;
  MEM ;
  NOT ;
  IF { PUSH string ""ONLY FOR SIGNERS"" ; FAILWITH } {} ;
  DUP 5 ;
  DUP 2 ;
  CDR ;
  CDR ;
  SIZE ;
  COMPARE ;
  GE ;
  NOT ;
  IF { PUSH string ""NOT YET APPROVED"" ; FAILWITH } {} ;
  PUSH mutez 0 ;
  AMOUNT ;
  COMPARE ;
  EQ ;
  NOT ;
  IF { PUSH string ""TO FUND CONTRACT, PLEASE USE THE DEFAULT ENTRYPOINT"" ; FAILWITH } {} ;
  DUP 7 ;
  DUP 3 ;
  GET ;
  IF_NONE { PUSH string ""pending_ops"" ; PUSH string ""ASSET_NOT_FOUND"" ; PAIR ; FAILWITH } {} ;
  CDR ;
  CAR ;
  PUSH unit Unit ;
  EXEC ;
  DIP { DIG 8 ; DROP } ;
  DUG 8 ;
  DUP 7 ;
  NONE (pair timestamp (pair (lambda unit (list operation)) (set address))) ;
  DUP 4 ;
  UPDATE ;
  DIP { DIG 6 ; DROP } ;
  DUG 6 ;
  DROP 2 ;
  PAIR 6 ;
  DIG 1 ;
  PAIR".

Definition ep_archive_expired_operation : string := "DUP 6 ;
  DUP 2 ;
  GET ;
  IF_NONE { PUSH string ""INVALID OP ID"" ; FAILWITH } {} ;
  DUP 4 ;
  SENDER ;
  MEM ;
  NOT ;
  IF { PUSH string ""ONLY FOR SIGNERS"" ; FAILWITH } {} ;
  DUP 5 ;
  DUP 2 ;
  CDR ;
  CDR ;
  SIZE ;
  COMPARE ;
  LT ;
  NOT ;
  IF { PUSH string ""CANNOT ARCHIVE APPROVED OPERATION"" ; FAILWITH } {} ;
  NOW ;
  DUP 2 ;
  CAR ;
  COMPARE ;
  LT ;
  NOT ;
  IF { PUSH string ""OPERATION NOT YET EXPIRED"" ; FAILWITH } {} ;
  PUSH mutez 0 ;
  AMOUNT ;
  COMPARE ;
  EQ ;
  NOT ;
  IF { PUSH string ""TO FUND CONTRACT, PLEASE USE THE DEFAULT ENTRYPOINT"" ; FAILWITH } {} ;
  DUP 7 ;
  NONE (pair timestamp (pair (lambda unit (list operation)) (set address))) ;
  DUP 4 ;
  UPDATE ;
  DIP { DIG 6 ; DROP } ;
  DUG 6 ;
  DROP 2 ;
  PAIR 6 ;
  DIG 1 ;
  PAIR".

Definition contract_s : string := "storage (pair (address %owner)
              (pair (set %signers address)
                    (pair (nat %threshold)
                          (pair (nat %last_op_id)
                                (pair
                                  (big_map %pending_ops nat
                                                        (pair (timestamp %expiration)
                                                              (pair
                                                                (lambda %actions unit
                                                                                 (list operation))
                                                                (set %approvals address))))
                                  (big_map %metadata string bytes))))));
parameter (or (or (bytes %set_metadata_uri) (unit %default))
              (or
                (or (pair %propose (lambda %the_actions unit (list operation)) (int %the_duration))
                    (nat %approve))
                (or (nat %execute) (nat %archive_expired_operation))));
code { NIL operation ;
       DIG 1 ;
       UNPAIR ;
       DIP { UNPAIR 6 } ;
       IF_LEFT
         { IF_LEFT {" ++ ep_set_metadata_uri ++ "} {" ++ ep_default ++ "} }
         { IF_LEFT
             { IF_LEFT {" ++ ep_propose ++ "} {" ++ ep_approve ++ "} }
             { IF_LEFT {" ++ ep_execute ++ "} {" ++ ep_archive_expired_operation ++ "} } } }".
