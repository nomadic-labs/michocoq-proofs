Require Import String ZArith Uint63.

From Michocoq Require Import syntax_type syntax entrypoints comparable.
Require Import contract_semantics.

Definition action_ty : type := lambda unit (list operation).
Definition approvals_ty : type := set address.
Definition proposal_ty : type := pair timestamp (pair action_ty approvals_ty).
Definition owner_ty : type := address.
Definition signers_ty : type := set address.
Definition threshold_ty : type := nat.
Definition op_id_ty : comparable_type := nat.
Definition pending_ops_ty : type := big_map op_id_ty proposal_ty.
Definition metadata_ty : type := big_map string bytes.

Definition storage_ty : type :=
  pair owner_ty
       (pair signers_ty
             (pair threshold_ty
                   (pair op_id_ty
                         (pair pending_ops_ty
                               metadata_ty)))).

Definition set_metadata_uri_annot :=
  Some (Mk_field_annotation "%set_metadata_uri" I).

Definition default_annot :=
  Some (Mk_field_annotation "%default" I).

Definition propose_annot :=
  Some (Mk_field_annotation "%propose" I).

Definition approve_annot :=
  Some (Mk_field_annotation "%approve" I).

Definition execute_annot :=
  Some (Mk_field_annotation "%execute" I).

Definition archive_expired_operation_annot :=
  Some (Mk_field_annotation "%archive_expired_operation" I).

Definition parameter_ty :=
  ep_node
    (ep_node
       (ep_leaf bytes)
       set_metadata_uri_annot
       (ep_leaf unit) default_annot)
    None
    (ep_node
       (ep_node
          (ep_leaf
             (pair action_ty int))
          propose_annot
          (ep_leaf op_id_ty)
          approve_annot) None
       (ep_node
          (ep_leaf op_id_ty)
          execute_annot
          (ep_leaf op_id_ty)
          archive_expired_operation_annot)
       None)
    None.

Definition update_metada_bigmap key (value : Datatypes.option (comparable_data bytes)) old_map new_map :=
  new_map =
    map.update (compare_eq_iff string) (lt_trans string) (gt_trans string)
               key value old_map.

Record storage : Set :=
  Mk_storage
    {
      owner : data owner_ty;
      signers : data signers_ty;
      threshold : data threshold_ty;
      last_op_id : comparable_data op_id_ty;
      pending_ops : data pending_ops_ty;
      metadata : data metadata_ty
    }.

Inductive parameter : Set :=
| Set_metadata_uri : data bytes -> parameter
| Default : parameter
| Propose : data action_ty -> data int -> parameter
| Approve : data op_id_ty -> parameter
| Execute : data op_id_ty -> parameter
| Archive_expired_operation : data op_id_ty -> parameter.

Definition set_metadata_uri_spec
           self_type (env : @proto_env self_type)
           (arg : data bytes)
           (old_storage : storage)
           (emmited_operations : data (list operation))
           (new_storage : storage) : Prop :=
  env.(sender) = old_storage.(owner) /\
  (* env.(amount) = 0%uint63 /\ *)
  emmited_operations = nil /\
  new_storage.(owner) = old_storage.(owner) /\
  new_storage.(signers) = old_storage.(signers) /\
  new_storage.(threshold) = old_storage.(threshold) /\
  new_storage.(last_op_id) = old_storage.(last_op_id) /\
  new_storage.(pending_ops) = old_storage.(pending_ops) /\
  update_metada_bigmap ""%string (Some arg) old_storage.(metadata) new_storage.(metadata).

Definition default_spec
           self_type (env : @proto_env self_type)
           (old_storage : storage)
           (emmited_operations : data (list operation))
           (new_storage : storage) : Prop :=
  emmited_operations = nil /\
  new_storage = old_storage /\
  (old_storage.(threshold) <= N.of_nat (set.size old_storage.(signers)))%N.

Definition storage_invariant_spec (old_storage new_storage : storage) : Prop :=
  new_storage.(owner) = old_storage.(owner) /\
  new_storage.(signers) = old_storage.(signers) /\
  new_storage.(threshold) = old_storage.(threshold) /\
  new_storage.(metadata) = old_storage.(metadata).

Definition propose_spec
           self_type (env : @proto_env self_type)
           (action : data action_ty) (duration : data int)
           (old_storage : storage)
           (emmited_operations : data (list operation))
           (new_storage : storage) : Prop :=
  env.(amount) = 0%uint63 /\
  (duration > 0)%Z /\
  let no_approvals : data approvals_ty := set.empty in
  let approvals : data approvals_ty :=
      update_v _ _ _ (Update_variant_set address) env.(sender) true no_approvals
  in
  let proposal : data proposal_ty :=
      ((env.(now) + duration)%Z, (action, approvals))
  in
  let new_op_id := (old_storage.(last_op_id) + 1)%N in
  mem_v _ _ (Mem_variant_set address) env.(sender) old_storage.(signers) = true /\
  map.mem new_op_id old_storage.(pending_ops) = false /\
  storage_invariant_spec old_storage new_storage /\
  emmited_operations = nil /\
  new_storage.(pending_ops) = update_v _ _ _ (Update_variant_bigmap op_id_ty proposal_ty) new_op_id (Some proposal) old_storage.(pending_ops) /\
  new_storage.(last_op_id) = new_op_id.

Definition approve_spec
           self_type (env : @proto_env self_type)
           (arg : comparable_data op_id_ty)
           (old_storage : storage)
           (emmited_operations : data (list operation))
           (new_storage : storage) : Prop :=
  env.(amount) = 0%uint63 /\
  mem_v _ _ (Mem_variant_set address) env.(sender) old_storage.(signers) = true /\
  storage_invariant_spec old_storage new_storage /\
  emmited_operations = nil /\
  exists old_proposal,
    get_v _ _ _ (Get_variant_bigmap op_id_ty proposal_ty) arg old_storage.(pending_ops) = Some old_proposal /\
    let '(timeout, (action, old_approvals)) := old_proposal in
    mem_v _ _ (Mem_variant_set address) env.(sender) old_approvals = false /\
    (now env < timeout)%Z /\
    let new_approvals : data approvals_ty :=
        update_v _ _ _ (Update_variant_set address) env.(sender) true old_approvals
    in
    let new_proposal : data proposal_ty := (timeout, (action, new_approvals)) in
    new_storage.(pending_ops) = update_v _ _ _ (Update_variant_bigmap op_id_ty proposal_ty) arg (Some new_proposal) old_storage.(pending_ops) /\
    new_storage.(last_op_id) = old_storage.(last_op_id).

Definition execute_spec
           self_type (env : @proto_env self_type) fuel
           (arg : comparable_data op_id_ty)
           (old_storage : storage)
           (emmited_operations : data (list operation))
           (new_storage : storage) : Prop :=
  env.(amount) = 0%uint63 /\
  mem_v _ _ (Mem_variant_set address) env.(sender) old_storage.(signers) = true /\
  storage_invariant_spec old_storage new_storage /\
  exists proposal : data proposal_ty,
    get_v _ _ _ (Get_variant_bigmap op_id_ty proposal_ty) arg old_storage.(pending_ops) = Some proposal /\
    let '(_timeout, (build_lam _ _ _ action, approvals)) := proposal in
    (N.of_nat (size_v _ (Size_variant_set address) approvals) >= old_storage.(threshold))%N /\
    new_storage.(last_op_id) = old_storage.(last_op_id) /\
    new_storage.(pending_ops) = map.remove (comparable.lt_trans nat) arg old_storage.(pending_ops) /\
    match (eval_seq (no_self env) action fuel (tt, tt)) with
    | error.Return (operations, tt) => emmited_operations = operations
    | _ => Logic.False
    end.

Definition archive_expired_operation_spec
           self_type (env : @proto_env self_type)
           (arg : data op_id_ty)
           (old_storage : storage)
           (emmited_operations : data (list operation))
           (new_storage : storage) : Prop :=
  env.(amount) = 0%uint63 /\
  mem_v _ _ (Mem_variant_set address) env.(sender) old_storage.(signers) = true /\
  storage_invariant_spec old_storage new_storage /\
  new_storage.(last_op_id) = old_storage.(last_op_id) /\
  emmited_operations = nil /\
  exists proposal : data proposal_ty,
    get_v _ _ _ (Get_variant_bigmap op_id_ty proposal_ty) arg old_storage.(pending_ops) = Some proposal /\
    let '(timeout, (_action, approvals)) := proposal in
    (N.of_nat (size_v _ (Size_variant_set address) approvals) < old_storage.(threshold))%N /\
    (timeout < now env)%Z /\
    new_storage.(pending_ops) = map.remove (comparable.lt_trans nat) arg old_storage.(pending_ops).

Definition read_storage (sto : data storage_ty) : storage :=
  let '(owner,
        (signers,
         (threshold,
          (last_op_id,
           (pending_ops,
            metadata)))))
      := sto
  in
  {|
    owner := owner;
    signers := signers;
    threshold := threshold;
    last_op_id := last_op_id;
    pending_ops := pending_ops;
    metadata := metadata
  |}.

Definition read_parameter (param : data (entrypoint_tree_to_type parameter_ty)) : parameter :=
  match param with
  | inl (inl arg) =>
    Set_metadata_uri arg
  | inl (inr tt) =>
    Default
  | inr (inl (inl (action, duration))) =>
    Propose action duration
  | inr (inl (inr arg)) =>
    Approve arg
  | inr (inr (inl arg)) =>
    Execute arg
  | inr (inr (inr arg)) =>
    Archive_expired_operation arg
  end.

Definition contract_specification
           self_type (env : @proto_env self_type) fuel
           (param : data (entrypoint_tree_to_type parameter_ty))
           (old_storage : data storage_ty)
           (emmited_operations : data (list operation))
           (new_storage : data storage_ty) : Prop :=
  let old_storage := read_storage old_storage in
  let new_storage := read_storage new_storage in
  match read_parameter param with
  | Set_metadata_uri arg =>
    set_metadata_uri_spec
      self_type env arg old_storage emmited_operations new_storage
  | Default =>
    default_spec
      self_type env old_storage emmited_operations new_storage
  | Propose action duration =>
    propose_spec
      self_type env action duration
      old_storage emmited_operations new_storage
  | Approve arg =>
    approve_spec
      self_type env arg old_storage emmited_operations new_storage
  | Execute arg =>
    execute_spec
      self_type env fuel arg old_storage emmited_operations new_storage
  | Archive_expired_operation arg =>
    archive_expired_operation_spec
      self_type env arg old_storage emmited_operations new_storage
  end.
