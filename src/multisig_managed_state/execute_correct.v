(* Open Source License *)
(* Copyright (c) 2022 Nomadic Labs. <contact@nomadic-labs.com> *)

(* Permission is hereby granted, free of charge, to any person obtaining a *)
(* copy of this software and associated documentation files (the "Software"), *)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense, *)
(* and/or sell copies of the Software, and to permit persons to whom the *)
(* Software is furnished to do so, subject to the following conditions: *)

(* The above copyright notice and this permission notice shall be included *)
(* in all copies or substantial portions of the Software. *)

(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER *)
(* DEALINGS IN THE SOFTWARE. *)

Require Import ZArith.

From Michocoq Require Import syntax.
From Michocoq Require parser_helpers.
Require parsing.
Require specification.
Require Import contract_semantics.

Definition storage_ty := parsing.storage_ty.
Definition parameter_ty := parsing.parameter_ty.
Definition self_ty := parsing.self_ty.

Definition ep_input_stack := parsing.entrypoint_input_stack.

Definition ep_annot := specification.execute_annot.

Definition ep_parameter_ty := Eval cbv in
      error.opt_extract
        (entrypoints.get_entrypoint_opt ep_annot parameter_ty None)
        I.

Definition ep_M : error.M (instruction_seq self_ty false (ep_parameter_ty ::: ep_input_stack) (pair (list operation) storage_ty ::: nil)) :=
  Eval cbv in
     parser_helpers.string_to_instr_seq script_string.ep_execute 500 _ _ _.

Definition ep : instruction_seq self_ty false (ep_parameter_ty ::: ep_input_stack) (pair (list operation) storage_ty ::: nil) :=
  Eval cbv in error.extract ep_M I.

Lemma execute_correct fuel env (new_storage : data storage_ty) (emmited_operations : data (list operation)) (arg : data ep_parameter_ty) (old_storage : data storage_ty) :
  let '(owner,
        (signers,
         (threshold,
          (last_op_id,
           (pending_ops,
            metadata))))) := old_storage in
  eval_seq_precond (S (S fuel)) env ep (fun SA => SA = ((emmited_operations, new_storage), tt)) (arg, (owner, (signers, (threshold, (last_op_id, (pending_ops, (metadata, (nil, tt)))))))) <->
  specification.execute_spec
    self_ty env (S fuel) arg
    (specification.read_storage old_storage)
    emmited_operations
    (specification.read_storage new_storage).
Proof.
  simpl.
  destruct old_storage as (owner, (signers, (threshold, (last_op_id, (pending_ops, metadata))))).
  unfold specification.execute_spec,
  specification.storage_invariant_spec.
  destruct new_storage as (owner', (signers', (threshold', (last_op_id', (pending_ops', metadata'))))).
  simpl.

  (* I don't know why but "repeat rewrite base.ex_and_comm_left" does nothing *)
  match goal with |- _ <-> _ /\ _ /\ ?A /\ exists _, _ => progress rewrite (base.ex_and_comm_left _ A) end.
  match goal with |- _ <-> _ /\ ?A /\ exists _, _ => progress rewrite (base.ex_and_comm_left _ A) end.
  match goal with |- _ <-> ?A /\ exists _, _ => progress rewrite (base.ex_and_comm_left _ A) end.
  apply forall_ex; intros (deadline, ((tff, action), approvals)).

  match goal with
    |- _ <-> ?A /\ ?B /\ (?C /\ ?D /\ ?E /\ ?F) /\ ?G /\ ?H /\ ?I =>
    transitivity (G /\ B /\ H /\ A /\ C /\ D /\ E /\ F /\ I); [|intuition]
  end.

  apply base.and_both_left_impl; intro Hget.
  rewrite Hget.
  rewrite base.ex_eq_some_simpl.

  rewrite Bool.negb_false_iff.
  apply base.and_both_left.

  rewrite Bool.negb_false_iff.
  rewrite comparable.geb_ge_nat.
  apply base.and_both_left.

  rewrite Bool.negb_false_iff.
  rewrite (comparable.eqb_eq mutez).
  apply base.and_both_left.

  progress change (@eval_precond_body (@eval_precond ?fuel)) with (@eval_precond (S fuel)).
  progress change (@eval_seq_precond_body (@eval_precond ?fuel)) with (@eval_seq_precond fuel).
  rewrite <- eval_seq_precond_correct.
  unfold error.precond.

  destruct eval_seq as [err|(returned_ops, [])]; simpl.
  - intuition.
  - split.
    + intros H. injection H. intros. subst.
      simpl.
      intuition.
    + intros (H1, (H2, (H3, (H4, (H5, (H6, H7)))))).
      subst.
      reflexivity.
Qed.
